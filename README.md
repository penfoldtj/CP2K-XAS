<table align="center">
<tr><td align="center" width="10000">


# <strong> CP2K X-ray Spectroscopy Examples </strong>

<p>
    <a href="https://www.cp2k.org/exercises:2019_conexs_newcastle:index">CP2K Examples </a>
</p>

</td></tr></table>

#

This repository provides example calculations for X-ray spectroscopy within CP2K. 

Further details about CP2K can be found:
* T. Kühne, et al. "CP2K: An electronic structure and molecular dynamics software package-Quickstep: Efficient and accurate electronic structure calculations." The Journal of Chemical Physics 152.19 (2020).


The theory behind X-ray spectroscopy implementations can be found in:
* M. Iannuzzi "X-ray absorption spectra of hexagonal ice and liquid water by all-electron Gaussian and augmented plane wave calculations." Journal of Chemical Physics 128.20 (2008).
* A. Bussy and J. Hutter. "First-principles correction scheme for linear-response time-dependent density functional theory calculations of core electronic states." Journal of Chemical Physics 155.3 (2021).
* A. Bussy, and J. Hutter. "Efficient and low-scaling linear-response time-dependent density functional theory implementation for core-level spectroscopy of large and periodic systems." Physical Chemistry Chemical Physics 23.8 (2021): 4736-4746.
* N Velasquez et al. X-ray induced ultrafast charge transfer in thiophene-based conjugated polymers controlled by core-hole clock spectroscopy Physical Chemistry Chemical Physics 26 (2), 1234-1244 (2024)


## Getting Started

The quickest way to get started this is to clone this repository:


<!---
```
git clone https://gitlab.com/penfoldtj/CP2K-XAS.git 
```
--->

```
git clone https://gitlab.com/penfoldtj/CP2K-XAS.git 
```

This contains all the source files as well as example input files.


## Questions

Feel free to get in touch if you have any questions at tom.penfold@newcastle.ac.uk

